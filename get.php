<?php

require './conexao.php';

if ('GET' === $_SERVER['REQUEST_METHOD']) {

    $response = [];
    $response['status'] = 'ok';
    $response['comentarios'] = [];

    $qtd = 10;

    $mysqli = conectar();
    $stmt = $mysqli->prepare('SELECT id, nome, texto, ts_criacao FROM comentario ORDER BY ts_criacao DESC LIMIT ?');
    $stmt->bind_param('i', $qtd);
    $stmt->execute();
    $stmt->bind_result($id, $nome, $texto, $ts_criacao);
    $stmt->store_result();
    while ($stmt->fetch()) {
        $response['comentarios'][] = ['id' => $id, 'nome' => $nome, 'texto' => $texto, 'ts_criacao' => $ts_criacao];
    }
    $stmt->free_result();
    $stmt->close();
    $mysqli->close();

    header('Content-Type: application/json;charset=utf-8');
    echo json_encode($response);
} else {
    header('HTTP/1.0 405 Method Not Allowed');
    return;
}
