<?php

// DSV
define('BD_HOST', 'localhost');
define('BD_USERNAME', 'root');
define('BD_PASSWD', '');
define('BD_DBNAME', 'php-app');

// PRD
//define('BD_HOST', 'localhost');
//define('BD_USERNAME', 'root');
//define('BD_PASSWD', 'ADMemk04177');
//define('BD_DBNAME', 'php');

function conectar(): mysqli {
    $mysqli = new mysqli(BD_HOST, BD_USERNAME, BD_PASSWD, BD_DBNAME);
    $mysqli->set_charset('utf8');
    return $mysqli;
}
