<?php

require './conexao.php';

if ('POST' === $_SERVER['REQUEST_METHOD']) {

    $response = [];
    $response['status'] = 'init';
    $response['msgs'] = [];

    $nome = filter_input(INPUT_POST, 'nome');
    if ($nome === false || $nome === NULL || strlen(trim($nome)) < 5 || strlen(trim($nome)) > 250) {
        $response['status'] = 'erro_validacao';
        array_push($response['msgs'], 'Inválido: Nome. Informe entre 5 e 250 caracteres.');
    }

    $texto = filter_input(INPUT_POST, 'texto');
    if ($texto === false || $texto === NULL || strlen(trim($texto)) < 5 || strlen(trim($texto)) > 1000) {
        $response['status'] = 'erro_validacao';
        array_push($response['msgs'], 'Inválido: Texto. Informe entre 5 e 1000 caracteres.');
    }

    if (count($response['msgs']) == 0) {

        $mysqli = conectar();
        $stmt = $mysqli->prepare('INSERT INTO comentario (nome, texto) VALUES (?, ?)');
        $stmt->bind_param('ss', $nome, $texto);
        $stmt->execute();
        $stmt->close();
        $mysqli->close();

        $response['status'] = 'ok';
    }

    header('Content-Type: application/json;charset=utf-8');
    echo json_encode($response);
} else {
    header('HTTP/1.0 405 Method Not Allowed');
    return;
}
